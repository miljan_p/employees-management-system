Zadatak
 

Koristeći principe OOP implementirati sistem za rukovođenje radnicima, njihovim statusima i isplatom.
Svaki radnik ima ime, prezime i adresu (Grad, ulica i broj). Radnik može biti zaposlen ili nezaposlen. Zaposleni radnici mogu da budu na godišnjem odmoru 
ili u stanju rada.
Promena svakog od stanja se loguje u fajl ili konzolu.

Radnik može da bude programer, doktor ili radnik za studentske poslove. Svaki radnik moze da primi platu, ode na odmor i vrati se sa odmora.
Svaki od zaposlenih je zaposlen kod svog poslodavca. Poslodavac poseduje ime i struku koju zapošljava (programer / doktor / studentski posao). 
Poslodavac definiše platu svakog od radnika.
Zaposleni u istoj instituciji ne moraju da imaju istu platu. Plata doktora se određuje na mesečnom nivou (randomizovati broj od 400 do 2000). 





Plate programera i studentski poslova se definišu satnicama. Za studentski posao uzeti random broj od 3 do 7, 
dok za programere uzeti random broj od 10 do 100 (to su satnice). Kada student ili programer ode na godišnji odmor, 
on ne biva plaćen, dok kod doktora odmor ne utiče na isplatu.
Kod radnika koji nisu zaposleni, ukoliko dođe do pokušaja primanja plate (tačka 8.), baciti izuzetak uz propratnu poruku: 
“Ne možete da primite platu jer niste zaposleni”.

Simulirati rad ovog sistema na sledeći način:
1)	Kreirati 4 poslodavca, jednog za studentske poslove, jednog za doktore i dva za programere.
2)	Kreirati 40 nezaposlenih ljudi - 20 programera, 10 doktora i 10 studenata (imena i prezimena nisu bitna, generisati ih).
3)	Svakom poslodavcu pridruziti po pet ljudi u svojoj struci i dodeliti vrednost plate
4)	Logovati datum i vreme zaposlenja u formatu DD/MM/YYYY HH:mm
5)	Simulirati odlazak na odmor od 10 dana za svakog od radnika iz svake institucije.
6)	Izračunati i logovati platu svakog od radnika nakon mesec dana (30 dana), računajući da je od tih 30 dana 10 dana bio odmor svakom radniku. Ukoliko je radnik nezaposlen, baciti izuzetak sa odgovarajućom propratnom porukom.
7)	Promeniti stanje svih studenata u stanje nezaposlen.
8)	Promeniti struku polovini studenata u programere, a drugoj polovini u doktore 

