package DAO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;

import javax.swing.JOptionPane;

import model.Identifiable;

public interface DAOinterface {
	
	public static final String payBehavioursByHourPath = "payBehavioursByHour.txt";
	public static final String payBehavioursByMonthPath = "payBehavioursByMonth.txt"; 
	public static final String addresesPath = "addresses.txt";
	public static final String workersPath = "workers.txt";
	public static final String employersPath = "employers.txt";
	public static final String institutionsPath = "institutions.txt";
	public static final String hiringLogsPath = "hiringLogs.txt";
	
	public static final String temporaryFilePath = "temp.txt";
	
	
	public static HashMap<Integer,Identifiable> ucitajSve(CreateFromStringInterface createFromString, String path) {
		
		var mapa = new HashMap<Integer,Identifiable>();
		
		BufferedReader reader = null;
		
		try {
			reader = FileUtils.getReader(path);
		
			String line = "";
			while((line = reader.readLine()) != null  ) {
				
				if(!line.equals("")) {
					var object = createFromString.CreateFromString(line);
					mapa.put(object.getId(), object);
				} else {
					continue;
				}
			}
		
			reader.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,e.getMessage());
			System.out.println("Gre�ka");
			e.getStackTrace();
		}
		return mapa;
	}
	
	
	public static void dodaj(Identifiable object, String path) throws IOException{
		
		
		BufferedWriter writer = null;
		try {
			writer = FileUtils.getWriter(path,  true);
			writer.append(object.WriteToString());
			writer.newLine();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();			
		} finally {
			try {
				writer.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();			
			}

		}

	}
	
	
	
	public static void izmeni(Identifiable object, String path) throws IOException{

		
		try (BufferedReader reader = FileUtils.getReader(path); 
			BufferedWriter writer = FileUtils.getWriter(temporaryFilePath, false)) {
			
			
			String line = "";
			while ((line = reader.readLine()) != null) {
				if (line.isEmpty()) {
					continue;
				}
				if (object.getId() == getIdFromString(line)) {
					writer.write(object.WriteToString());
					writer.newLine();
				} else {
					writer.write(line);
					writer.newLine();
				}
			} 
			
			}catch(FileNotFoundException e) {
				e.printStackTrace();
			}catch(IOException e) {
				e.printStackTrace();
			
			}
		
		File original = FileUtils.getFileForName(path);
	
		File backUpFile = new File("backUp");
		
		Files.move( original.toPath(), backUpFile.toPath(),
                StandardCopyOption.REPLACE_EXISTING );
		
		File temporary = FileUtils.getFileForName(temporaryFilePath);
		temporary.renameTo(original);
		backUpFile.delete();
	
	}
	
	
	public static void dodajLog(LogInterface object, String path) throws IOException{
		
		
		BufferedWriter writer = null;
		try {
			writer = FileUtils.getWriter(path,  true);
			writer.append(object.writeLogToFile());
			//writer.newLine();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
			
		} finally {
			try {
				writer.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
				
			}

		}
	}
	

	
	
	private static int getIdFromString(String text) throws NumberFormatException {
		int id = 0;
		String[] a = text.split("\\|");
		id = Integer.parseInt(a[0]);
		return id;
	}
	
	
}
