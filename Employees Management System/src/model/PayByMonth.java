package model;

import java.util.StringJoiner;

public class PayByMonth extends Identifiable implements PayBehaviour {
	
	private static final double MONTH = 30.0;
	private double min, max;
	
	public PayByMonth(int id, double max, double min) {
		super(id);
		
		this.min = min;
		this.max = max;
	}

	public PayByMonth() {
		this(-1, -1, -1);
	}
	
	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	@Override
	public void determineSalary(Worker worker) {
		
		worker.setSalary((Math.random()*((max-min)+1))+min);  
	}
	
	@Override
	public double calculateSalary(int daysOfWork, double salary) {
		return (salary * daysOfWork) / MONTH;
	}
	
	
	public static Identifiable CreateFromString(String text) {
		var payByMonth = new PayByMonth();
		
		String[] payByMonthParts = text.trim().split("\\|");
		
		payByMonth.setId(Integer.valueOf(payByMonthParts[0]));
		payByMonth.setMax(Double.valueOf(payByMonthParts[1]));
		payByMonth.setMin(Double.valueOf(payByMonthParts[2]));
		
		return payByMonth;
	}

	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getMax()+"").add(this.getMin()+"");
		
		return line.toString();
	}
	
	@Override
	public String toString() {
		return "Mesecno placanje";
	}
	
	
}
