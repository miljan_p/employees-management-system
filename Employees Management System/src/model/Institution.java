package model;

import java.util.ArrayList;
import java.util.StringJoiner;

import controller.DataStore;

public class Institution extends Identifiable {
	
	private String name;
	private ArrayList<Employer> employers;
	private ArrayList<Worker> workers;
	
	
	public Institution(int id, String name, ArrayList<Employer> employers, ArrayList<Worker> workers) {
		super(id);
		this.name = name;
		this.employers = employers;
		this.workers = workers;
	}


	public Institution() {
		this(-1, "", null, null);
	}


	public ArrayList<Employer> getEmployers() {
		return employers;
	}


	public void setEmployers(ArrayList<Employer> employers) {
		this.employers = employers;
	}


	public ArrayList<Worker> getWorkers() {
		return workers;
	}


	public void setWorkers(ArrayList<Worker> workers) {
		this.workers = workers;
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public static Identifiable CreateFromString(String text) {
		var institution = new Institution();
		
		String[] institutionParts = text.trim().split("\\|");
		
		institution.setId(Integer.valueOf(institutionParts[0]));
		institution.setName(institutionParts[1]);
		institution.setEmployers(DataStore.findEmployersForInstitution(institution.getId()));
		institution.setWorkers(DataStore.findWorkersForInstitution(institution.getId()));
		
	
		return institution;
	}


	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getName());
		return line.toString();
	}
	
	
	public String display() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID Institucije: " + this.getId()+"\n");
		sb.append("\tBroj poslodavaca: " + this.getEmployers().size()+"\n");
		sb.append("\tBroj radnika: " + this.getWorkers().size()+"\n");
		
		return  sb.toString();
	}
	
	
	
	
}
