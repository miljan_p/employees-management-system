package model;

import DAO.WriteToStringInterface;

public abstract class Identifiable implements WriteToStringInterface{
	
	protected int id;
	
	public Identifiable(int id) {
		super();
		this.id = id;
	}
	
	public Identifiable() {
		this(-1);
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		
		return this.id;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identifiable other = (Identifiable) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
