package model;

@SuppressWarnings("serial")
public class CannotReceiveSalaryException extends Exception {
	 
	public CannotReceiveSalaryException(String errorMessage) {
	        super(errorMessage);
	    }
}
