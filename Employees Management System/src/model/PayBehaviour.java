package model;

public interface PayBehaviour{
	
	public void determineSalary(Worker worker);
	public double calculateSalary(int daysOfWork, double salary);
	
}
