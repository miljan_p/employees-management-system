package model;

public enum Profession {
	
	PROGRAMMER,
	DOCTOR,
	STUDENT,
	UNDETERMINE;
	
	private String text;
	
	static {
		PROGRAMMER.text = "Programmer";
		DOCTOR.text = "Doctor";
		STUDENT.text = "Student";
		UNDETERMINE.text = "Undetermine";
	}
	
	public String getText() {
		return text;
	}
	
	public static Profession getProfession(int ord) {
		switch(ord) {
			case 0: return Profession.PROGRAMMER;
			case 1: return Profession.DOCTOR;
			case 2 : return Profession.STUDENT;
			default: return Profession.UNDETERMINE; 	
		}
	
	}
	
	
	@SuppressWarnings("unlikely-arg-type")
	public String professionToFileString() {
		if (this.text.equals("Programmer")) {
			return "0";
		} else if (this.text.equals("Doctor")) {
			return "1";
		} else if (this.equals("Student")) {
			return "2";
		} else return "-1";
	}

}
