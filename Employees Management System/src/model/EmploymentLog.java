package model;

import java.util.Date;

import DAO.LogInterface;
import controller.DataStore;
import utility.Utility;

public class EmploymentLog implements LogInterface{
	
	
	private Employer employer;
	private Worker worker;

	public EmploymentLog(Employer employer, Worker worker) {
		this.employer = employer;
		this.worker = worker;
	}
	
	
	@Override
	public String writeLogToFile() {
		
		StringBuilder sb = new StringBuilder();
		//[]
		String action = "[Zaposlenje]";
		
		String dateOfHiring = "[Datum zaposlenja: " + Utility.convertDateWithTimeToString(new Date()) + "]";
		
		String employer = "[Poslodavac: " + this.employer.getName() + "]";
		
		String worker = "[Radnik: " + this.worker.getName() + "]";
		
		String occupation = "[Zanimanje radnika: " + this.worker.getProfesion().getText() + "]";
		
		String institution = "[Institucija: " + ((Institution) DataStore.institutions.get(this.employer.getInstitution())).getName() +"]";
		
		sb.append(action + " " + dateOfHiring + " " + employer + " " + worker + " " + occupation + " " + institution+"\n");
		
		return sb.toString();
		
	}


	public Employer getEmployer() {
		return employer;
	}


	public void setEmployer(Employer employer) {
		this.employer = employer;
	}


	public Worker getWorker() {
		return worker;
	}


	public void setWorker(Worker worker) {
		this.worker = worker;
	}
	
	
	
	
}
