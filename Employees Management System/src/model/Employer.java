package model;

import java.util.StringJoiner;

import controller.DataStore;
import utility.Utility;

public class Employer extends Identifiable {
	
	private String name;
	private Profession hiringProfession;
	private int institution;
	
	public Employer(int id, String name, Profession hiringProfession, int institution) {
		super(id);
		this.name = name;
		this.hiringProfession = hiringProfession;
		this.institution = institution;
	}


	public Employer() {
		this(-1, "", Profession.UNDETERMINE, -1);
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Profession getHiringProfession() {
		return hiringProfession;
	}


	public void setHiringProfession(Profession hiringProfession) {
		this.hiringProfession = hiringProfession;
	}


	public int getInstitution() {
		return institution;
	}


	public void setInstitution(int institution) {
		this.institution = institution;
	}


	public boolean hire(Worker newWorker)  {
		
		if (newWorker.getProfesion() == this.getHiringProfession()) {
			
			addToInstitution(newWorker);
			determineSalaryForWorker(newWorker);
			return true;
		}
		
		return false;
	}
	
	private void determineSalaryForWorker(Worker newWorker) {
		if (newWorker.getProfesion() == this.hiringProfession) {
			
			int max = Utility.ocitajCeoBroj("Unesi gornji limit plate: ");
			int min = Utility.ocitajCeoBroj("Unesi donji limit plate: ");
			PayBehaviour newPayBehaviour;
			EmploymentLog employmentLog;
			int newId;
			
			if (newWorker.getProfesion() == Profession.DOCTOR) {
				
				newId = DataStore.generateId(DataStore.payBehavioursByMonth);				
				newPayBehaviour = new PayByMonth(newId, max, min);
				
				DataStore.payBehavioursByMonth.put(((PayByMonth)newPayBehaviour).getId(), ((PayByMonth)newPayBehaviour));
				try {
					
					employmentLog = new EmploymentLog(this, newWorker);
					DataStore.dodajLog(employmentLog);
					DataStore.dodaj((PayByMonth)newPayBehaviour);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
	
			} else {
				employmentLog = new EmploymentLog(this, newWorker);
				
				newId = DataStore.generateId(DataStore.payBehavioursByHour);
				newPayBehaviour = new PayByHour(newId, max, min);
				DataStore.payBehavioursByHour.put(((PayByHour)newPayBehaviour).getId(), ((PayByHour)newPayBehaviour));
				
				try {
					DataStore.dodajLog(employmentLog);
					DataStore.dodaj((PayByHour)newPayBehaviour);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			} 
			
			newWorker.setEmploymentStatus(EmploymentStatus.EMPLOYED);
			newWorker.setWorking(true);
			newWorker.setEmployeer(this.getId());
			newWorker.setPayBehaviour(newPayBehaviour);
			newPayBehaviour.determineSalary(newWorker);
			
			try {
				DataStore.izmeni(newWorker);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		
	}
	
	
	private void addToInstitution(Worker newWorker) {
		var institution = (Institution) DataStore.institutions.get(this.institution);
		institution.getWorkers().add(newWorker);
		newWorker.setInstitution(this.getInstitution());
	}
	
	public static Identifiable CreateFromString(String text) {
		var employer = new Employer();
		
		String[] employerParts = text.trim().split("\\|");
		
		employer.setId(Integer.valueOf(employerParts[0]));
		employer.setName(employerParts[1]);
		employer.setHiringProfession(Profession.getProfession(Integer.valueOf(employerParts[2])));
		employer.setInstitution((Integer.valueOf(employerParts[3])));
		
		return employer;
	}


	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getName()).add(this.getHiringProfession().professionToFileString())
		.add(this.getInstitution()+"");
		
		return line.toString();
	}
	
	
	public String display() {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("ID poslodvaca: " + this.getId()+"\n");
		sb.append("\tIme poslodavca: " + this.getName()+"\n");
		sb.append("\tTip radnika koji zapos�ljava: " + this.getHiringProfession().getText()+"\n");
		sb.append("\tInstitucija: " +((Institution)DataStore.institutions.get(this.getInstitution())).getName()+"\n");
		
		return sb.toString();
		
		
		
		
	}
	
}
