package model;

import java.util.StringJoiner;

public class Address extends Identifiable {
	
	private String city;
	private String street;
	private int number;
	
	
	
	public Address(int id, String city, String street, int number) {
		super(id);
		this.city = city;
		this.street = street;
		this.number = number;
	}

	public Address() {
		this(-1, "", "",-1);
		
	}

	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	
	
	public static Identifiable CreateFromString(String text) {
		var address = new Address();
		
		String[] addressParts = text.trim().split("\\|");
		
		address.setId(Integer.valueOf(addressParts[0]));
		address.setCity(addressParts[1]);
		address.setStreet(addressParts[2]);
		address.setNumber(Integer.valueOf(addressParts[3]));
		
		return address;
	}


	@Override
	public String WriteToString() {
		
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getCity()).add(this.getStreet()).add(this.getNumber()+"");
		
		return line.toString();
		
	
	}
	
	
}
