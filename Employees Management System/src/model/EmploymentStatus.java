package model;

public enum EmploymentStatus {
	
	EMPLOYED,
	NOT_EMPLOYED,
	UNDETERMINE;
	
	private String text;
	
	static {
		EMPLOYED.text = "Employed";
		NOT_EMPLOYED.text = "Not Employed";
		UNDETERMINE.text = "Undetermine";
	}
	
	public String getText() {
		return text;
	}
	
	public static EmploymentStatus getEmploymentStatus(int ord) {
		switch(ord) {
			case 0: return EmploymentStatus.EMPLOYED;
			case 1: return EmploymentStatus.NOT_EMPLOYED;
			default : return EmploymentStatus.UNDETERMINE;
		}
	
	}
	
	public String employmentStatusToFileString() {
		if (this.text.equals("Employed")) {
			return "0";
		} else if (this.text.equals("Not Employed")) {
			return "1";
		} else return "-1";
	}
	
}
