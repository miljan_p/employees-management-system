package model;

import java.util.StringJoiner;

import controller.DataStore;

public class Worker extends Identifiable {
	
	protected String name;
	protected String lastName;
	protected Address address;
	protected boolean working;
	protected int employeer;
	protected Profession profesion;
	protected EmploymentStatus employmentStatus;
	protected PayBehaviour payBehaviour;
	protected int institution;
	protected double salary;
	
	
	
	
	public Worker(int id, String name, String lastName, Address address, boolean working, int employeer,
			Profession profesion, EmploymentStatus employmentStatus, PayBehaviour payBehaviour, int institution, double salary) {
		super(id);
		this.name = name;
		this.lastName = lastName;
		this.address = address;
		this.working = working;
		this.employeer = employeer;
		this.profesion = profesion;
		this.employmentStatus = employmentStatus;
		this.payBehaviour = payBehaviour;
		this.institution = institution;
		this.salary = salary;
	}
	
	public Worker() {
		this(-1, "", "", null, false, -1, Profession.UNDETERMINE, EmploymentStatus.UNDETERMINE, null, -1, -1);
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Address getAddress() {
		return address;
	}


	public void setAddress(Address address) {
		this.address = address;
	}


	public boolean isWorking() {
		return working;
	}


	public void setWorking(boolean working) {
		this.working = working;
	}


	public int getEmployeer() {
		return employeer;
	}


	public void setEmployeer(int employeer) {
		this.employeer = employeer;
	}


	public EmploymentStatus getEmploymentStatus() {
		return employmentStatus;
	}


	public void setEmploymentStatus(EmploymentStatus employmentStatus) {
		this.employmentStatus = employmentStatus;
	}


	public PayBehaviour getPayBehaviour() {
		return payBehaviour;
	}


	public void setPayBehaviour(PayBehaviour payBehaviour) {
		this.payBehaviour = payBehaviour;
	}


	public Profession getProfesion() {
		return profesion;
	}


	public void setProfesion(Profession profesion) {
		this.profesion = profesion;
	}	
	
	public int getInstitution() {
		return institution;
	}

	public void setInstitution(int institution) {
		this.institution = institution;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	

	public double calculateSalary(int daysOfWork) {
		return this.getPayBehaviour().calculateSalary(daysOfWork, this.getSalary());
	}
	

	public static Identifiable CreateFromString(String text) {
		var worker = new Worker();
		
		String[] workerParts = text.trim().split("\\|");
		
		worker.setId(Integer.valueOf(workerParts[0]));
		worker.setName(workerParts[1]);
		worker.setLastName(workerParts[2]);
		worker.setAddress((Address) DataStore.addresses.get(Integer.valueOf(workerParts[3])));
		worker.setWorking(Boolean.valueOf(workerParts[4]));
		worker.setEmployeer(Integer.valueOf(workerParts[5]));
		worker.setProfesion(Profession.getProfession(Integer.valueOf(workerParts[6])));
		worker.setEmploymentStatus(EmploymentStatus.getEmploymentStatus(Integer.valueOf(workerParts[7])));
		
		int idPayBehaviour = Integer.valueOf(workerParts[8]);
		if (idPayBehaviour == -1) worker.setPayBehaviour(null);			
		else worker.setPayBehaviour(DataStore.getPayBehaviour(idPayBehaviour, worker.getProfesion()));
		
		worker.setSalary(Double.valueOf(workerParts[9]));
		worker.setInstitution(Integer.valueOf(workerParts[10]));
				
		return worker;
		
	}

	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		String textPayBehaviour;
		if (this.getPayBehaviour() == null) textPayBehaviour = "-1";
		else textPayBehaviour = String.valueOf(((Identifiable) this.getPayBehaviour()).getId());
		
		
		line.add(this.getId()+"").add(this.getName()).add(this.getLastName()).add(this.getAddress().getId()+"")
		.add(this.isWorking()+"").add(this.getEmployeer()+"").add(this.getProfesion().professionToFileString()).add(this.getEmploymentStatus().employmentStatusToFileString())
		.add(textPayBehaviour).add(this.getSalary()+"").add(this.getInstitution()+"");
		
		
		return line.toString();
	}
	
	public String display() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID radnika: " + this.getId()+"\n");
		sb.append("\tIme: " + this.getName()+"\n");
		sb.append("\tProfesija: " + this.getProfesion().getText()+"\n");
		sb.append("\tStatus zaposlenja: " + this.getEmploymentStatus().getText()+"\n");
		if (this.getPayBehaviour() != null) sb.append("\tNacin placanja: " + this.getPayBehaviour().toString()+"\n");
		
		if (this.getInstitution() != -1) sb.append("\tInstitucija: " + ((Institution)DataStore.institutions.get(this.getInstitution())).getName()+"\n");
	
		
		return sb.toString();
	}
	
	
	
}
