package model;

import java.util.StringJoiner;

public class PayByHour extends Identifiable implements PayBehaviour{
	private static final double HOURS_PER_DAY = 8.0;
	//private static final double DAYS_PER_WEEK = 5.0;
	//private static final double WEEKS_PER_MONTH = 4.0;
	private static final double DAYS_OF_WORK_FOR_ONE_DAY_OFF = 3;
	private double max, min;

	public PayByHour(int id, double max, double min) {
		super(id);
		this.min = min;
		this.max = max;
	}
	
	public PayByHour() {
		this(-1, -1, -1);
	}
	
	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	@Override
	public void determineSalary(Worker worker) {
		
		worker.setSalary((Math.random()*((max-min)+1))+min);  
	}
	
	@Override
	public double calculateSalary(int daysOfWork, double salary) {
		
		double dailySalary = salary*HOURS_PER_DAY;
		
		double daysOfNetWork = daysOfWork / DAYS_OF_WORK_FOR_ONE_DAY_OFF;
		
		return dailySalary*daysOfNetWork;
	}
	
	public static Identifiable CreateFromString(String text) {
		var payByHour = new PayByHour();
		
		String[] payByHourParts = text.trim().split("\\|");
		
		payByHour.setId(Integer.valueOf(payByHourParts[0]));
		payByHour.setMax(Double.valueOf(payByHourParts[1]));
		payByHour.setMin(Double.valueOf(payByHourParts[2]));
		
		return payByHour;
	}

	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getMax()+"").add(this.getMin()+"");
		
		return line.toString();
	}
	
	@Override
	public String toString() {
		return "Placanje po stanu";
	}

	
}
