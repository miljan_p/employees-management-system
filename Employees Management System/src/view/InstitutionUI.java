package view;

import controller.DataStore;
import display.Display;
import model.Employer;
import model.Institution;
import model.Profession;
import model.Worker;
import utility.Utility;

public class InstitutionUI {
	
	
	public static void goToInstitutionMenu() {
		
		
		if (DataStore.institutions.size() == 0) {
			System.out.println("Trenutno ne postoji nijedna institucija. Morate napraviti novu instituciju.");
			return;
		}
		
		Institution currentInstitution = Display.chooseInstitution("Izberite instituciju unoseci ID institucije.");
		
		
		while(true) {
			System.out.println("1. Dodaj nove poslodavce u instituciju\n2. Po�alji zaposlene na odmor\n3. Povratak u prethodni meni\n0. Izlazak iz aplikacije");
			
			int izbor = Utility.choice(3);
			
			switch(izbor) {
				case 1:
					addNewEmployers(currentInstitution);
					break;
				case 2:
					sendEmployeesToVacation(currentInstitution);
					break;
				case 3:
					return;
				case 0:
					System.exit(1);
			}
		}
	}//od metode
	
	
	public static void sendEmployeesToVacation(Institution currentInstitution) {
		
		for (Worker w : currentInstitution.getWorkers()) {
			w.setWorking(false);
			try {
				DataStore.izmeni(w);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Zaposlni u ovoj instituciji se od sada nalaze na odmoru.");
		
	}
	
	public static void addNewEmployers(Institution currentInstitution) {
		var newEmployer = new Employer();
		
		newEmployer.setId(DataStore.generateId(DataStore.employers));
		newEmployer.setName(Utility.ocitajTekst("Unesite ime novog poslodavca: "));
		newEmployer.setHiringProfession(chooseHiringProfession());
		newEmployer.setInstitution(currentInstitution.getId());
		
		currentInstitution.getEmployers().add(newEmployer);
		DataStore.employers.put(newEmployer.getId(), newEmployer);
		try {
			DataStore.dodaj(newEmployer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Novi poslodavac uspe�no dodat u instituciju.");
	}
	
	private static Profession chooseHiringProfession() {
		
	
		System.out.println("Izaberite profesiju koju poslodavac moze da zapo�ljava.");
		System.out.println("1. Programer\n2. Doktor\n0. Student");
		int izbor = Utility.choice(2);
		while (true) {
			switch(izbor) {
				case 1: return Profession.PROGRAMMER;
				case 2: return Profession.DOCTOR;
				case 0: return Profession.STUDENT;
				default: System.out.println("Izaberite odgovarajucu vrednost.");
			}
		}
		
	}
	
	
	
	
	public static void createFirstInstitution() {
		//
	}
	
	
	
	
	
	
	
	
	

} //od klase
