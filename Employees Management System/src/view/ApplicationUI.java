package view;

import java.util.ArrayList;

import controller.DataStore;
import model.Address;
import model.Employer;
import model.EmploymentStatus;
import model.Institution;
import model.Profession;
import model.Worker;
import utility.Utility;

public class ApplicationUI {
	
	public static final int MAX_NAME_LENGTH = 10; //duzina imena i prezimena Worker-a ce biti izmedju 5 i 10
	public static final int MIN_NAME_LENGTH = 5;
	public static ArrayList<Worker> employmentBureau = new ArrayList<Worker>();
	
	public static void main(String[] args) {
		
		System.out.println();
		DataStore.initialLoad();
		System.out.println();
	
		while(true) {
			System.out.println("1. Napravi nove nazaposlene radnike\n2. Napravi novu instituciju\n3. Institutcija\n4. Poslodavac\n0. Izlazak iz aplikacije");
			
			int izbor = Utility.choice(4);
			
			switch (izbor) {
				case 1:
					makeNewUnemployedWorkers();
					break;
				case 2:
					makeNewInstitution();
					break;
				case 3:
					InstitutionUI.goToInstitutionMenu();
					break;
				case 4:
					EmployerUI.goToEmployerMenu();
					break;
				case 0:
					System.exit(1);
			}
		}
		
	}
	
	
	public static void makeNewInstitution() {
		var newInstitution = new Institution();
		
		newInstitution.setId(DataStore.generateId(DataStore.institutions));
		newInstitution.setName(Utility.ocitajTekst("Unesite ime nove institucije: "));
		newInstitution.setEmployers(new ArrayList<Employer>());
		newInstitution.setWorkers(new ArrayList<Worker>());
		
		DataStore.institutions.put(newInstitution.getId(), newInstitution);
		try {
			DataStore.dodaj(newInstitution);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Uspe�no dodata nova institucija.");
	}
	
	
	
	public static void makeNewUnemployedWorkers() {
		System.out.println("Izaberite tip i kolicinu novi radnik.");
		while (true) {
			System.out.println("1. Programer\n2. Student\n3. Doktor\n0. Povratak u prethodni meni");
			
			int izbor = Utility.choice(3);
			int amount;
			
			switch (izbor) {
				case 1:
					amount = Utility.ocitajCeoBroj("Unesite kolicinu: ");
				
					createWorkers(amount, Profession.PROGRAMMER);
					break;
				case 2:
					amount = Utility.ocitajCeoBroj("Unesite kolicinu: ");
					
					createWorkers(amount, Profession.STUDENT);
					break;
				case 3:
					amount = Utility.ocitajCeoBroj("Unesite kolicinu: ");
					
					createWorkers(amount, Profession.DOCTOR);
					break;
				case 0:
					return;
			}
		}
		
		
	}
	
	private static void createWorkers(int amount, Profession profession) {
		for (int i = 0; i < amount; i++) {
			var newWorker = new Worker();
			newWorker.setId(DataStore.generateId(DataStore.workers));
			newWorker.setName(nameGenerator(generateRandomLength()));
			newWorker.setLastName(nameGenerator(generateRandomLength()));
			newWorker.setAddress(generateAddressObject());
			newWorker.setWorking(false);
			newWorker.setEmployeer(-1);
			newWorker.setProfesion(profession);
			newWorker.setEmploymentStatus(EmploymentStatus.NOT_EMPLOYED);
			newWorker.setPayBehaviour(null);
			newWorker.setSalary(0);
			newWorker.setInstitution(-1);
			
			DataStore.workers.put(newWorker.getId(), newWorker);
		
			try {
				DataStore.dodaj(newWorker);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			employmentBureau.add(newWorker);
		}
	}
	
	private static Address generateAddressObject() {
		var newAddress = new Address();
		newAddress.setId(DataStore.generateId(DataStore.addresses));
		newAddress.setCity("Novi Sad");
		newAddress.setStreet(nameGenerator(generateRandomLength()));
		newAddress.setNumber((int) (100*Math.random()));
		
		DataStore.addresses.put(newAddress.getId(), newAddress);
		
		try {
			DataStore.dodaj(newAddress);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return newAddress;
	
	}
	
	
	public static String nameGenerator(int length) {
		
		String AlphaString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		
		StringBuilder sb = new StringBuilder(length);
		
		 for (int i = 0; i < length; i++) { 
			  
	            // generate a random number between 
	            // 0 to AlphaNumericString variable length 
	            int index = (int)(AlphaString.length() * Math.random()); 
	                 
	                        
	  
	            // add Character one by one in end of sb 
	            sb.append(AlphaString.charAt(index)); 
	        } 
	  
	        return sb.toString(); 

	}
	
	private static int generateRandomLength() {
		return (int) ((Math.random()*((MAX_NAME_LENGTH-MIN_NAME_LENGTH)+1))+MIN_NAME_LENGTH);
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
