package view;

import controller.DataStore;
import display.Display;
import model.Employer;
import model.Worker;
import utility.Utility;

public class EmployerUI {
	
	public static void goToEmployerMenu() {
		
		if (DataStore.employers.size() == 0) {
			System.out.println("Morate dodati barem jednog poslodavca u instituciju.");
			return;
		}
		
		
		Employer currentEmployer = Display.chooseEmployer("Izaberite poslodavca unoseci njegov ID broj.");
		
	
		
		
		while(true) {
			System.out.println("1. Zaposli radnike i odredi im platu\n0. Povratak u prethodni meni");
			int izbor = Utility.choice(1);
			switch(izbor) {
			case 1: 
				hireNewWorkers(currentEmployer);
				break;
			case 0:
				return;
			
			}
		}
	}
	
	
	public static void hireNewWorkers(Employer currentEmployer) {
		Worker choosenWorker = Display.chooseUnemployedWorker("Izberite radnika kojeg zelite da zaposlite unoseci ID radnika.");
		
		if (choosenWorker == null) {
			return;
		}
		
		
		if (currentEmployer.getHiringProfession() != choosenWorker.getProfesion()) {
			System.out.println("Nazalost, za izabranog radnika nemate privilegije zapošljavanja. Pokušajte ponovo");
			return;
		}
		
		ApplicationUI.employmentBureau.remove(choosenWorker);
		
		if (currentEmployer.hire(choosenWorker)) {
			System.out.println("Uspešno ste zaposlili radnika");
		} else {
			System.out.println("Nažalost zapošljavanje radnika nije uspelo.");
		}
		
		
		return;
	
	
	
	}
	
	
	
	
	
	
	
	
}
