package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

import DAO.DAOinterface;
import DAO.LogInterface;
import model.Address;
import model.Employer;
import model.EmploymentLog;
import model.Identifiable;
import model.Institution;
import model.PayBehaviour;
import model.PayByHour;
import model.PayByMonth;
import model.Profession;
import model.Worker;

public class DataStore {
	
	/*
	 * All objects (with established references) are stored here, in these global HashMaps
	 */
	
	public static HashMap<Integer, Identifiable> addresses = null;
	public static HashMap<Integer, Identifiable> payBehavioursByHour = null;
	public static HashMap<Integer, Identifiable> payBehavioursByMonth = null;
	public static HashMap<Integer, Identifiable> workers = null;
	public static HashMap<Integer, Identifiable> employers = null;
	public static HashMap<Integer, Identifiable> institutions = null;
	
	
	public static void initialLoad() {
		
		/*
		 * Loading all object and establishing references. Since all Identifiable objects
		 * have override CreateFromString method, each object whill now how to behave, 
		 * at runtime of course.
		 */
		
		addresses = DAOinterface.ucitajSve(Address::CreateFromString, DAOinterface.addresesPath);
		
		payBehavioursByHour =  DAOinterface.ucitajSve(PayByHour::CreateFromString, DAOinterface.payBehavioursByHourPath);
		
		payBehavioursByMonth = DAOinterface.ucitajSve(PayByMonth::CreateFromString, DAOinterface.payBehavioursByMonthPath);
		
		workers = DAOinterface.ucitajSve(Worker::CreateFromString, DAOinterface.workersPath);
		
		employers = DAOinterface.ucitajSve(Employer::CreateFromString, DAOinterface.employersPath);
	
		institutions =  DAOinterface.ucitajSve(Institution::CreateFromString, DAOinterface.institutionsPath);
		
	}
	
	/*
	 * When creating any kind of Identifiable object, we are making sure that
	 * that particular objects gets unique ID number, under which acts like unique ID in the global map
	 * for those kinds of objects.
	 */
	
	public static int generateId(HashMap<Integer, Identifiable> list) {
		if (!list.isEmpty()) {
			return list.values().stream().map(i -> i.getId()).max(Integer::compare).get() + 1;
		} else
			return 0;
	}
	
	public static PayBehaviour getPayBehaviour(int idPayBehaviour, Profession profession) {
		if (profession == Profession.DOCTOR) {
			return (PayBehaviour) payBehavioursByMonth.get(idPayBehaviour);
		} 
		return (PayBehaviour) payBehavioursByHour.get(idPayBehaviour);
	}
	
	
	public static ArrayList<Employer> findEmployersForInstitution(int idInstitution){
		
		return new ArrayList<Employer>(employers.entrySet().stream()
				.filter(i -> ((Employer) i.getValue()).getInstitution() == idInstitution)
				.map(i -> ((Employer) i.getValue())).collect(Collectors.toList()));
		
	}
	
	public static ArrayList<Worker> findWorkersForInstitution(int idInstitution){
		return new ArrayList<Worker>(workers.entrySet().stream()
				.filter(i -> ((Worker) i.getValue()).getInstitution() == idInstitution)
				.map(i -> ((Worker) i.getValue())).collect(Collectors.toList()));
	}
	
	
	
	public static void dodaj(Identifiable object) throws Exception {
		
		//nisam fan ovih f-ja
		if (object instanceof Worker) {
			DAOinterface.dodaj(object, DAOinterface.workersPath);
		} else if (object instanceof Address) {
			DAOinterface.dodaj(object, DAOinterface.addresesPath);
		} else if (object instanceof Employer) {
			DAOinterface.dodaj(object, DAOinterface.employersPath);
		} else if (object instanceof Institution) {
			DAOinterface.dodaj(object, DAOinterface.institutionsPath);
		} else if (object instanceof PayByHour) {
			DAOinterface.dodaj(object, DAOinterface.payBehavioursByHourPath);
		} else if (object instanceof PayByMonth) {
			DAOinterface.dodaj(object, DAOinterface.payBehavioursByMonthPath);
		} 
	}
	
	
	public static void izmeni(Identifiable object) throws Exception {
		
		//nisam fan ovih f-ja
		if (object instanceof Worker) {
			DAOinterface.izmeni(object, DAOinterface.workersPath);
		} else if (object instanceof Address) {
			DAOinterface.izmeni(object, DAOinterface.addresesPath);
		} else if (object instanceof Employer) {
			DAOinterface.izmeni(object, DAOinterface.employersPath);
		} else if (object instanceof Institution) {
			DAOinterface.izmeni(object, DAOinterface.institutionsPath);
		} else if (object instanceof PayByHour) {
			DAOinterface.izmeni(object, DAOinterface.payBehavioursByHourPath);
		} else if (object instanceof PayByMonth) {
			DAOinterface.izmeni(object, DAOinterface.payBehavioursByMonthPath);
		} 
	}
	
	
	public static void dodajLog(LogInterface logObject) throws Exception {
		if (logObject instanceof EmploymentLog) {
			DAOinterface.dodajLog(logObject, DAOinterface.hiringLogsPath);
		}
	}
	
	
	
}
