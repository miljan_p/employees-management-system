package display;

import controller.DataStore;
import model.Institution;
import model.Worker;
import utility.Utility;
import view.ApplicationUI;
import model.Employer;

public class Display {
	
	
	//klase koja ima helper static metode koje ce se koristiti za 
		//prikaz i/ili izbor raznih elemenata, listi, objekata itd.
		
	
	public static Institution chooseInstitution(String text) {
		
		System.out.println(text);
		
		
		
		for (Integer key : DataStore.institutions.keySet()) {
			Institution ins = (Institution) DataStore.institutions.get(key);
			System.out.println(ins.display());
		}
		
		while (true) {
			int idInstitution = Utility.ocitajCeoBroj("Unesite ID institucije: ");
			
			for (Integer key : DataStore.institutions.keySet()) {
				if (key == idInstitution) {
					return (Institution) DataStore.institutions.get(key);
				}
			}
			
			System.out.println("Unesite odgovarajuci ID institucije");
			
		}
		
	}
	
	public static Employer chooseEmployer(String text) {
		
		System.out.println(text);
		
		
		
		for (Integer key : DataStore.employers.keySet()) {
			Employer emp = (Employer) DataStore.employers.get(key);
			System.out.println(emp.display());
		}
		
		while (true) {
			int idEmployer = Utility.ocitajCeoBroj("Unesite ID poslodavca: ");
			
			for (Integer key : DataStore.employers.keySet()) {
				if (key == idEmployer) {
					return (Employer) DataStore.employers.get(key);
				}
			}
			
			System.out.println("Unesite odgovarajuci ID poslodavca.");			
		}
		
		
	}
	
	
	public static Worker chooseUnemployedWorker(String text) {
		
		if (ApplicationUI.employmentBureau.size() == 0) {
			System.out.println("Trenutno nema ne zaposlenih radnika na birou.");
			return null;
		}
		
		System.out.println(text);
		
		for (Worker uw : ApplicationUI.employmentBureau) {
			System.out.println(uw.display());
		}
		
		
		while (true) {
			int idWorker = Utility.ocitajCeoBroj("Unesite ID radnika: ");
			
			
			for (Worker uw : ApplicationUI.employmentBureau) {
				if (uw.getId() == idWorker) {
					return (Worker) DataStore.workers.get(idWorker);
				}
			}
			
			System.out.println("Unesite odgovarajuci ID radnika.");			
		}
		
		
	}
	
	
	
}
